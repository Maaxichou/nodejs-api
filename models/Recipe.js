
var mongoose = require('mongoose');

const route = 'recipe'; 	
const modelId = 'Recipe';  	

var Schema = new mongoose.Schema({

	name: String,
	category: String,
	picture: String,
	score: Integer

});

module.exports = {
	model: mongoose.model(modelId, Schema),
	route: route
} 