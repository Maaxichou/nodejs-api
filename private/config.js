
var crypto = require('crypto');

module.exports = {

	DB_PATH: 'mongodb://localhost:27017/mydatabase',
	PORT: 3000,

	SESSION_SECRET_KEYS: [
        crypto.randomBytes(32).toString('hex'), crypto.randomBytes(32).toString('hex'),
        crypto.randomBytes(32).toString('hex'), crypto.randomBytes(32).toString('hex')
    ],
}
