
var modelLocation = '../models/Recipe'

var util = require('util');
var express = require('express');
var bodyParser = require('body-parser');
var authController = require('./AuthController');

var model = require(modelLocation).model;

const route = require(modelLocation).route;
const routeIdentifier = util.format('/%s', route);

var router = express.Router();

 router.get(routeIdentifier+'/list', function(req, res, next) {
 	model.find(function (err, objects) {
 		if (err) return res.send(err);
 		return res.json(objects);
 	});
 });

 router.get(routeIdentifier+'/create', function(req, res, next) {
 	model.create(req.query, function (err, entry) {
 		if (err) return next(err);
 		return res.json({
            status: 'Success',
            message: 'Recipe created!'
        });
 	});
 });

 router.get(routeIdentifier+'/get/:id', function (req, res, next) {
 	model.findOne({
        '_id':req.params.id,
    }, function (err, entry){
 		if(err) return res.send(err);
 		return res.json(entry);
 	});
 });

 router.get(routeIdentifier+'/update/:id', function(req, res, next) {
 	model.findOneAndUpdate({
        '_id':req.params.id,
    },
    req.query,
    function (err, entry) {
 		if (err) return res.send(err);
 		return res.json({status: 'Success', message: 'Updated recipe'});
 	});
 });

router.get(routeIdentifier+'/delete/:id', function (req, res, next) {
  model.findOneAndRemove({
        '_id':req.params.id,
    },
    req.body,
    function (err, entry) {
        if (err) return res.send(err);
        return res.json({status: 'Success', message: 'Deleted recipe'});
    });
});

 module.exports = router;
