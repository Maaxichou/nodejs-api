var modelLocation = '../models/Ingredient'

var util = require('util');
var express = require('express');
var bodyParser = require('body-parser');

var model = require(modelLocation).model;

const route = require(modelLocation).route;
const routeIdentifier = util.format('/%s', route);


var router = express.Router();

 router.get(routeIdentifier+'/list', function(req, res, next) {
 	model.find({'recipe':req.user._id}, function (err, objects) {
 		if (err) return res.send(err);
 		return res.json(objects);
 	});
 });


 router.get(routeIdentifier+'/create', function(req, res, next) {
 	req.body.owner = req.user._id;
 	model.create(req.query, function (err, entry) {
 		if (err) return next(err);
 		return res.json({
            status: 'Success',
            message: 'Ingredient created!'
        });
 	});
 });


 router.get(routeIdentifier+'/get/:id', function (req, res, next) {
 	model.findOne({
        '_id':req.params.id,
        'recipe':req.recipe._id
    }, function (err, entry){
 		if(err) return res.send(err);
 		return res.json(entry);
 	});
 });



 router.get(routeIdentifier+'/update/:id', function(req, res, next) {
 	model.findOneAndUpdate({
        '_id':req.params.id,
        'recipe':req.recipe._id
    },
    req.query,
    function (err, entry) {
 		if (err) return res.send(err);
 		return res.json({status: 'Success', message: 'Updated Ingredient'});
 	});
 });


router.get(routeIdentifier+'/delete/:id', function (req, res, next) {
  model.findOneAndRemove({
        '_id':req.params.id,
        'recipe':req.recipe._id
    },
    req.body,
    function (err, entry) {
        if (err) return res.send(err);
        return res.json({status: 'Success', message: 'Deleted ingredient'});
    });
});

 module.exports = router;
